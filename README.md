# Test-Suite


A comprehensive test suite for Kubernetes cluster that can be used to run continuous testing both manually and through gitlab-ci for Benchmarking testing, Conformance testing, Functional testing and Resiliency Testing.
The testsuite is build on CNCF chaostesting project [Litmus](https://hub.litmuschaos.io/).

## Release



| Test-Suite | Litmus | Kubernetes |
| -------- | -------- | -------- |
| v2.0.0     | 3.4.0     | >1.27.0   |

[Release Notes](https://hedgedoc.tools.innov.intraorange/chXWH4UfQx2uPGTv5HThkQ) 


## Test Suite Architecture
![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/d5201f34-0767-4e1e-a79c-e16833a51ed8.png)


* The user interacts with Test Suite application using apis
* Using kubeconfig file of a k8s cluster we can register the cluster to execute experiments
* Once the cluster is registered, experiment apis can be used to create experiment according to users requirements.
* Probes can be used to run various cases like validating the experiment, test various validating steps at before, at and after the execution of the experiment.
* Only one experiment can be executed at a time on a particular cluster, but we can execute different experiments on different k8s cluster simultaneously
* Possibility to ==execute custom experiments==


### Test Suite High Level View:


#### Cluster
![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/32b2bf20-f5a9-4b72-ad04-90e8399f07ff.png)


----

### Experiment
![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/67b4a2d1-485f-43c0-a093-7a8b136d980d.png)


----

### Workflow
![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/add790e9-33d7-41cc-8592-3ddf998a669d.png)

## Test Suite Deployment

The test-suite application can be deployed using the manifest file [deploy-test-suite.yaml](https://gitlab.com/enilegu1/test-suite/-/blob/main/deploy-test-suite.yaml)

```kubectl apply -f https://gitlab.com/enilegu1/test-suite/-/blob/main/deploy-test-suite.yaml```


### List on testcases:
* [Benchmarking](#benchmarking-test-cases)
    * [Storage benchmarking](#longhorn-storage-benchmarking)
* [Conformance](#conformance-test-cases)
    * [Sonobuoy conformance](#sonobuoy-conformance-test)
* [Functional](#functional-test-cases)
    * [MySQL load test](#mysql-functional-test)
* [Resiliency](#resiliency-test-cases)
    * [Workload](#pod-level-experiments)
        * [pod-cpu-hog](#3-pod-cpu-hog)
        * [pod-memory-hog](#4-pod-memory-hog)
        * [pod-delete](#1-pod-delete)
    * [Infra](#node-level-experiments)
        * [node-restart](#5-node-restart)
        * [node-cpu-hog](#1-node-cpu-hog)
        * [node-memory-hog](#2-node-memory-hog)
        

## Usage
Discover essential APIs for orchestrating and executing test suite applications through Swagger, accessible via: http://<test_suite_server_ip>:<test_suite_server_port>/swagger/index.html

### Execute APIs using CLI
Usage commands can be found at [Test Suite User Guide](https://hedgedoc.tools.innov.intraorange/xH5WxvZSS2-2eqkTwcFmrA).

### Cluster Related APIs

![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/dd3ec007-ffe6-46fb-bfa5-702ed5adf2a7.png)

## Data Required for using APIs
### 1. Register Cluster
Registering Cluster for executing experiments

* Convert kubeconfig file to base64 using https://www.base64encode.org/ or using command
 ```encoded_config=$(cat cluster-kubeconfig-file | base64 -w 0)```

#### Register Cluster

```
{
    "clustername": "<cluster_name>",
    "clusterconfig":
    "<base64_encoded_kubeconfig_data>"
}
```
:::info
Note: Convert kubeconfig file to base64 using https://www.base64encode.org/
:::
    


## Experiment Related APIs

![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/8432388d-4baf-4d7a-9d90-4873f2b528d3.png)

### 1. List all available tests in suite
It will return a list of all available experiments that can be performed using the test suite


### 2. Fet Default and required parameters for experiments
It will return the default and required parameters required to register and run the experiment


### 3. Add Experiment to the test-suite
It will register experiment to the test-suite.


### 4. Fetch Experiment Details
It will list the details of an experiment by using its ID

### 5. Update Experiments
It can be used to update data in an experiment


### 6. Delete Experiments
It can be used to delete an experiment registered in the database.

## Workflow related APIs
These APIs are used to fetch workflow related details like executing the experiments and fetch its progress.

![](https://s3.flexible-datastore.orange-business.com/pr-diod-ocp-fr01-hedgedoc-s3-images-production/uploads/c81ad998-572f-4061-9cbf-b1d64dbf22a6.png)

### 1. Start a workflow
It will trigger an exeriment and return the workflow-id for the triggered experiment.

### 2. Fetch workflow status
It will fetch the details of a workflow

## Resiliency Test Cases

These testcases uses **[LitmusChaos](https://docs.litmuschaos.io/docs/getting-started/installation/)**, a framework for practicing Chaos Engineering on Kubernetes.

### List of experiments that can be performed using Resiliency Suite
#### **Node Level Experiments**

#####    1. [Node CPU Hog](https://hub.litmuschaos.io/kubernetes/node-cpu-hog)
Node CPU Hog is a chaos experiment designed to simulate high CPU usage scenarios within a Kubernetes cluster.


#####    2. [Node Memory Hog](https://hub.litmuschaos.io/kubernetes/node-memory-hog)
Chaos experiment triggering high memory consumption to evaluate Kubernetes cluster resilience and performance under memory-intensive conditions, essential for assessing system stability and resource handling capabilities.
#####    3. [Node Drain](https://hub.litmuschaos.io/kubernetes/node-drain)
Chaos  operation that gracefully evicts all pods from a node to simulate a scenario where the node is taken offline for maintenance or failure recovery.
#####    4. [Node IO Stress](https://hub.litmuschaos.io/kubernetes/node-io-stress)
Node IO Stress is a chaos engineering experiment that induces high I/O (Input/Output) load on a Kubernetes node to assess its resilience and performance under such stress conditions.
#####    5. [Node Restart](https://hub.litmuschaos.io/kubernetes/node-restart)


#####    6. [Node Taint](https://hub.litmuschaos.io/kubernetes/node-taint)
#####    7. [Node Poweroff](https://hub.litmuschaos.io/kubernetes/node-poweroff)

#### **Pod Level Experiments**
#####    1. Pod Delete
#####    2. Pod DNS Error
#####    3. Pod CPU Hog
#####    4. Pod Memory Hog
#####    5. Pod Network Corruption
#####    6. Pod Network Latency
#####    7. Pod Network Loss
#####    8. Pod Network Duplication
#####    9. Pod IO Stress


## Conformance Test Cases
[Conformance testing](https://portal.etsi.org/Services/Centre-for-Testing-Interoperability/ETSI-Approach/Conformance) in Kubernetes verifies that a cluster adheres to the specifications defined by the Kubernetes project.

### Sonobuoy Conformance Test
[Sonobuoy](https://sonobuoy.io/) is a diagnostic tool for running conformance tests in Kubernetes clusters. It provides a framework for running these tests, collecting results, and analyzing them to ensure that the cluster conforms to Kubernetes specifications.

## Functional Test Cases
Functional test cases are specific scenarios or actions performed on a software application to verify its functional requirements and features, ensuring that it behaves as expected according to the specifications.

### MySQL Functional Test
It involves verifying various aspects of its functionality, such as database creation, data manipulation (insert, update, delete), querying data using SQL statements, transaction management, user management, and ensuring data integrity and security measures are properly implemented.

## Benchmarking Test Cases
Benchmarking testing involves evaluating the performance, reliability, and scalability of software systems or components by subjecting them to controlled conditions and measuring their response against predetermined criteria.

### Longhorn Storage Benchmarking

[Longhorn](https://longhorn.io/blog/performance-scalability-report-aug-2020/) storage benchmarking involves assessing the performance and reliability of Longhorn storage systems under various conditions, such as different workloads, data sizes, and access patterns. This typically includes measuring metrics like throughput, latency, IOPS (Input/Output Operations Per Second), and data consistency to evaluate the storage system's suitability for specific use cases and workload requirements.







